<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class film extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['title','year','description','category_id'];

//    public function category()
//    {
//        return $this->belongsTo(Category::class);
//    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }




}


